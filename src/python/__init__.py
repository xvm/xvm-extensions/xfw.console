"""
This file is part of the XVM Framework project.

Copyright (c) 2018-2022 XVM Team.

XVM Framework is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, version 3.

XVM Framework is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
"""



#xfw native
from xfw_native.python import XFWNativeModuleWrapper



#
# Private
#

__xfw_console = None
__xfw_initialized = False



#
# XFW
#

def xfw_is_module_loaded():
    return __xfw_initialized


def xfw_module_init():
    global __xfw_console
    __xfw_console = XFWNativeModuleWrapper('com.modxvm.xfw.console', 'xfw_console.pyd', 'XFW_Console')
    __xfw_console.hook_install()

    global __xfw_initialized
    __xfw_initialized = True
