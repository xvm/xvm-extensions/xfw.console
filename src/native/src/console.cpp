/**
 * This file is part of the XVM Framework project.
 * Copyright (c) 2012-2012 Hedger.
 * Copyright (c) 2014-2014 Roger Sanders.
 * Copyright (c) 2017-2020 XVM Team.
 *
 * XVM Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XVM Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "console.h"

#include <chrono>
#include <iostream>
#include <thread>

#include <Windows.h>
#include <process.h>

#include <pybind11/eval.h>

bool Console::isActive = false;


unsigned __stdcall Console::ThreadConsole(void* args)
{
    std::string cmd;
    std::string batch;

    if (!Console::allocate()) {
        return 1;
    }


    printf("XVM Framework Console\n");
    printf("https://modxvm.com/\n");
    printf("\n");
    printf("Type `unload` to exit.\n");
    printf("\n");

    runPythonCode(
        "import os;"
        "import sys;"
        "old_stdout = sys.stdout;"
        "old_stderr = sys.stderr;"
        "logger = open(\"CONOUT$\", \"wt\");"
        "sys.stdout = logger;"
        "sys.stderr = logger;");

    isActive = true;

    _beginthreadex(nullptr, 0, &Console::threadFlusher, nullptr, 0, nullptr);

    while (true)
    {
        batch.clear();
        cmd.clear();

        printf(">>> ");
        do
        {
            std::getline(std::cin, cmd);
            if (!batch.empty() && !cmd.empty()) {
                batch.append("\n");
            }

            if (!cmd.empty()) {
                bool firstLine = batch.empty();
                batch.append(cmd);

                if (firstLine && (cmd.back() != ':'))
                {
                    break;
                }
                printf("... ");
            }
        } while (!cmd.empty());

        if (batch.compare("unload") == 0) {
            break;
        }

        runPythonCode(batch);
    }

    runPythonCode(
        "sys.stdout = old_stdout;"
        "sys.stderr = old_stderr;"
        "logger.close();"
    );

    isActive = false;

    if (!Console::release()) {
        return 2;
    }

    return 0;
}


bool Console::allocate()
{
    FreeConsole();

    if (!AllocConsole()){
        return false;
    }

    //disable close button
    HWND hwnd = GetConsoleWindow();
    if (hwnd != nullptr){
        HMENU hMenu = GetSystemMenu(hwnd, FALSE);
        if (hMenu != nullptr){
            DeleteMenu(hMenu, SC_CLOSE, MF_BYCOMMAND);
        }
    }

    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stdout);
    freopen("CONOUT$", "w", stderr);

    std::wcout.clear();
    std::cout.clear();
    std::wcerr.clear();
    std::cerr.clear();
    std::wcin.clear();
    std::cin.clear();

    SetConsoleTitleA("XVM Framework console");

    return true;
}

bool Console::release()
{
    return FreeConsole() != FALSE;
}

unsigned __stdcall Console::threadFlusher(void* args)
{
    while (isActive) {
        runPythonCode("");
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }

    return 0;
}


void Console::runPythonCode(const std::string& code)
{
    pybind11::gil_scoped_acquire gil_lock;

    PyRun_SimpleStringFlags(code.c_str(), 0);
    PyRun_SimpleStringFlags("sys.stdout.flush()", 0);
}
