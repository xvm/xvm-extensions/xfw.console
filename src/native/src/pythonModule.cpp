// SPDX-License-Identifier: LGPL-3.0+
// Copyright (c) 2012-2012 Hedger
// Copyright (c) 2014-2014 Roger Sanders.
// Copyright (c) 2017-2022 XVM Team


#include <process.h>

#include <pybind11/pybind11.h>

#include "console.h"
#include "keyboard.h"

PYBIND11_MODULE(XFW_Console, m) {
    m.doc() = "XFW Console module";

    m.def("open"        , [] {_beginthreadex(nullptr, 0, &Console::ThreadConsole, nullptr, 0, nullptr); }, "Open console window.");
    m.def("hook_install", [] {_beginthreadex(nullptr, 0, &Keyboard::ThreadHook  , nullptr, 0, nullptr); }, "Install keyboard hook.");
}
