/**
 * This file is part of the XVM Framework project.
 * Copyright (c) 2012-2012 Hedger.
 * Copyright (c) 2014-2014 Roger Sanders.
 * Copyright (c) 2017-2020 XVM Team.
 *
 * XVM Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * XVM Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

class Console {
public:

    static unsigned __stdcall ThreadConsole(void* args);

private:

    static bool allocate();

    static bool release();

    static unsigned __stdcall threadFlusher(void* args);

    static void runPythonCode(const std::string& code);

    static bool isActive;

};
